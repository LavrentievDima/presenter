(function ($) {
    $.fn.presenter = function (options) {
        let settings = $.extend({}, options);
        return this.each((i, el) => {
            let $this = $(el);
            let width = $this.outerWidth();
            let height = $this.outerHeight();
            $this.css({
                position: 'relative',
                border: '1px solid black',
                marginBottom: '60px',
                // zIndex: '2',
            });

            $this.wrap('<div class="presenter_wrapper">');
            let wrapper = $this.parent();
            wrapper.css({
                position: 'relative',
                background: 'black',
                // padding: '60px',
                overflow: 'auto',
                // zIndex: '-2',
            });
            $this.wrapInner('<div class="inner">');
            let inner = $this.find('.inner');
            inner.css({
                position: 'absolute',
                zIndex: '2',
                height: height,
                top: 0,
                left: 0,
                padding: `${$this.css('padding')}`,
                backgroundColor: `${$this.css('background-color')}`,
                border: '1px solid grey',
            });
            $this.append('<div class="stand">');
            let stand = $this.find('.stand');
            stand.css({
                position: 'absolute',
                width: `calc(100% + 60px)`,
                height: '50px',
                top: 'calc(100% - 25px)',
                left: '-30px',
                zIndex: '1',
                // border: '1px solid grey',
                '-moz-border-radius': '50%',
                '-webkit-border-radius': '50%',
                borderRadius: '50%',
                background: 'white',
                boxShadow: '0 0 80px 80px white'
            });
            $this.css({
                height: `${inner.css('height')}`,

            });

            wrapper.append('<div class="wall">');
            let wall = wrapper.find('.wall');
            wall.css({
                position: 'absolute',
                width: '100%',
                height: '470px',
                top: '0',
                left: '0',
                zIndex: '1',
                // border: '1px solid grey',
                background: 'rgba(255, 255, 255,.1)',
                boxShadow: 'rgb(0, 0, 0) 0px 12px 30px 0px',
            });
            wrapper.append('<div class="floor">');
            let floor = wrapper.find('.floor');
            floor.css({
                position: 'absolute',
                width: '100%',
                height: 'calc(100% - 470px)',
                top: '470px',
                left: '0',
                // zIndex: '1',
                // border: '1px solid grey',
                background: 'rgba(255, 255, 255,.1)',
            });


        });
    };
})(jQuery);
